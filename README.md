

### some notes
**thoughts:**

using tree structure with BFS as there aren't too many items I will have to deal within this exercise. 
Other-wise I would have used a map with an array or each item's parents for faster access.

**the process of work:**
* decide what data structure to use
* build a mock of it
* style the tree
* generate code that can reproduce the UI based on the mock data
* add logic based on useReducer
* build server as a mirror of the logic used on the client side
* refactor code - using custom hook for repetitive code 
* added mobx - as i saw it was a condition in the assignment : 
    * **note about mobx** : this is my first time using it, so it may not be written in best practice style. 

**Things I didn't finish:**

a bug in the UI when generating list B within  list A, when list A has other items after it. 

didn't have time to implement a general error handler
(errors are only logged to the dev tools when fetching data)

###the App
click to edit name
right click for option menu
