
function fetchWrapper(method, url, attr) {
    const urlPath = 'http://localhost:1000/category/' + url;

    return fetch(urlPath, {
        method: method,
        headers: {'Content-Type': 'application/json', Accept: 'application/json'},
        body: attr ? JSON.stringify(attr) : undefined,
    })
        .then(res=>res.json())
        .catch(e => console.log('error:', e));
}

export const xhr = {
    get(url="") {
        return fetchWrapper('get', url, undefined);
    },

    post(attr, url="") {
        return fetchWrapper('post', url, attr);
    },

    put(attr, url="") {
        return fetchWrapper('put', url, attr);
    },

    delete(attr, url="") {
        return fetchWrapper('delete', url, attr);
    },
};
