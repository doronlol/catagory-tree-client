import React from "react";

const SvgEdit = props => {
const { mainColor = "#FFF", secondaryColor="white", width = "15px", height ="15px"} = props;

    return (
        <svg
        viewBox="0 -5 34 34" 
        fill="none" 
        stroke="currentColor" 
        strokeWidth="2" 
        strokeLinecap="round" 
        strokeLinejoin="round" 
        x="0px"
        y="0px"
        width={width}
        height={height}
    >
        <g>
        <polygon fill={mainColor} points="14 2 18 6 7 17 3 17 3 13 14 2"></polygon>
        <line fill={mainColor} x1="3" y1="22" x2="21" y2="22"></line>
</g>
    </svg>
    );
}
export default SvgEdit;
