import React from "react";

const SvgPlus = props => {
const { mainColor = "#17607a", secondaryColor="white", width = "15px", height ="15px"} = props;

    return (
        <svg
        viewBox="0 0 24 24" 
        fill="none" 
        stroke="currentColor" 
        strokeWidth="2" 
        strokeLinecap="round" 
        strokeLinejoin="round" 
        x="0px"
        y="0px"
        width={width}
        height={height}
        // viewBox="0 0 241.59736 220.05746"
        // style="enable-background:new 0 0 30.239 30.239;"
    >
        <circle 
        fill={mainColor}
        stroke="none"
        cx="12" cy="12" r="10"></circle>
        <line stroke="white" x1="12" y1="8" x2="12" y2="16"></line>
        <line stroke="white" x1="8" y1="12" x2="16" y2="12"></line>
       
    </svg>
    );
}
export default SvgPlus;
