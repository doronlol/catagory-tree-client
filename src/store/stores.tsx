import React from 'react'
import {TreeStore, initialState} from './store'

const StoreContext = React.createContext({} as TreeStore);
export const store =  new TreeStore();

export const StoreProvider: React.FC<React.PropsWithChildren<{}>> = ({children})=>{
    return <StoreContext.Provider value={store}>
        {children}
    </StoreContext.Provider>
}

export const useRootStore = ()=> React.useContext(StoreContext)
