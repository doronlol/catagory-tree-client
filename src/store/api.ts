import {xhr} from '../services/xhr'

interface IData {
    name: string,
    parentId: string
}

export const getTree = ()=>{
    return xhr.get().then(res=>JSON.parse(res.data))
}

export const saveNode = (data:IData)=>{
    return xhr.post(data)
}

export const updateNode = (id: string, name:string)=>{
    return xhr.put({id, name})
}

export const removeNode = (id: string |undefined, parent: string)=>{
    return xhr.delete({id, parent})
}
