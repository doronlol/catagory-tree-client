import {observable, action, makeAutoObservable} from 'mobx';
import {getTree, removeNode, updateNode, saveNode} from './api'


export type itemType = "parent" | "child";

export const actionEnums = {
    ADD: "add",
    REMOVE: "remove",
    RENAME: "rename",
    FILL_TREE: "fillTree",
    TOGGLE: "toggle",
    ADD_CATEGORY: "addCategory"
}


export interface INode {
    type: itemType,
    name: string,
    children: INode[],
    id: string,
    parentId: string
}

export interface ICategory {
    name: string,
    parentId: string,
    type?: itemType
}

export const initialState: INode ={
    type: "parent",
    name: "",
    children: [],
    id: "0",
    parentId: ""
}

function find(root: INode, target: string | any){
    const queue = [root];

    while(queue.length){
        const current = queue.shift()!;
        if(current.id === target){
            return current;
        }
        if(current.children.length){
            current.children.forEach((child)=>queue.push(child))
        }

    }

    return false;

}


export class TreeStore{
    root:INode = initialState

    constructor() {
        makeAutoObservable(this,{
            root: observable,
            fillTree: action,
            addCategory: action,
            add: action,
            remove: action,
            rename: action,
        })

    }


    fillTree = async ()=>{
        const arr = await getTree();
        if(!arr || !Array.isArray(arr)) return;
        this.root.children = arr;
    }

    addCategory = async (node: ICategory)=>{
        const res: any = await saveNode(node);
        const newCategory: INode = {
            name: node.name,
            children: [],
            id: res.data.id,
            type: res.data.type? res.data.type : "child",
            parentId: node.parentId,
        }
        this.root.children.push(newCategory);
    }

    add = async (node: ICategory)=>{
        const res: any = await saveNode(node);
        const parent = find(this.root, node.parentId)
        if(!parent) return;


        const newCategory: INode = {
            name: node.name,
            children: [],
            id: res.data.id,
            type: node.type? node.type : "child",
            parentId: node.parentId,

        }

        parent.children.push(newCategory);
        return res;
    }
    remove = async (id: string | undefined, parentId: string)=>{
        const res: any = await removeNode(id, parentId);
        if(!res){
            return;
        }
        let parent = find(this.root, parentId)
        if(!parent){
            parent = this.root
        }

        const index = parent.children.findIndex(item=> item.id === id);
        if(index < 0) return;

        let newChildArr = [...parent.children]
        newChildArr.splice(index, 1)

        parent.children = newChildArr;
    }

    rename = async (id: string, name: string)=>{
        const wasUpdated: any = await updateNode(id,name);
        if(!wasUpdated.success){
            return;
        }
        const current = find(this.root, id)
        if(!current) return;

        current.name = name
    }

}
