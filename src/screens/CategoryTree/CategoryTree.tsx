import React, {useReducer, useEffect, useState} from 'react';
import styles from './CategoryTree.module.scss'
import SvgPlus from "../../assets/svgIcons/pluse";
import CategoryList from "./CategoryList";
import {INode} from '../../store/store'
import {useRootStore} from '../../store/stores'
import {useObserver} from 'mobx-react-lite'


function CategoryTree() {
  const [isNodeMenuOpen, setIsNodeMenuOpen] = useState<string>("");
  const [isServerDown, setIsServerDown] = useState<boolean>(false);
  const store = useRootStore()

  // On first load of the app
  useEffect(() => {
      store.fillTree().then(()=>{
          if(!isServerDown){
              setIsServerDown(false)
          }
          document.addEventListener("click", (event) => {setIsNodeMenuOpen("")});

          return ()=>{
              document.removeEventListener("click",(event) => {setIsNodeMenuOpen("")} );
          }

      }).catch((e:any)=> {
          console.log("error:", e);
          setIsServerDown(true)
      })

  }, []);

  const handleAdd= ()=>{
      if(isServerDown) return;

      store.addCategory({
          name: "new Category",
          parentId: "0"
      }).then(()=>{
          console.log("added category", store.root);
      })
    }

    const handleUpdateName = (id: string, name: string)=>{
      return store.rename(id, name)
    }

    const handleRemove = (id: string, parent: string)=>{
        return store.remove(id, parent)
    }

    return useObserver(()=>
    <div className={styles.tree_wrapper}>
        <div className={styles.tree_header}>
            <p className={styles.title}>Category List</p>
            <div
                className={styles.title_icon}
                onClick={()=>handleAdd()}
            >
                <SvgPlus width={"20px"} height={"20px"}/>
            </div>
        </div>

        <ul>
            {!isServerDown && store.root.children.map((child:INode)=>
                <ul key={child.id}>
                <CategoryList
                    key={child.id}
                    node={child}
                    isNodeMenuOpen={isNodeMenuOpen}
                    handleUpdateName={handleUpdateName}
                    handleRemove={handleRemove}
                    setIsNodeMenuOpen={setIsNodeMenuOpen}/>
                </ul>
                    )}
            {isServerDown && <div>
                <p>Server id down, please lift it and refresh the page</p>
            </div>}
        </ul>
    </div>
  )

}

export default CategoryTree;


