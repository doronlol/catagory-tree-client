import React from 'react';
import clsx from 'clsx';
import styles from './PopUpMenu.module.scss'
import {useRootStore} from '../../../store/stores'
import {itemType, actionEnums} from '../../../store/store'


interface IProps {
    onClose: ()=>void;
    setIsOpen: (value: boolean)=>void;
    parent: string;
    id?: string;
    oneItem?: boolean;
}

function ActionPopupMenu(props: IProps) {
    const store = useRootStore();

    const handleAction = (type: string, e:any, nodeType:itemType= "child") =>{
        e.stopPropagation()
        switch (type) {
            case actionEnums.ADD:
                store.add({
                    name: "new Category",
                    parentId: props.id || "0",
                    type: nodeType,
                }).then(()=>{
                    props.setIsOpen(true)
                    props.onClose()
                })



                return;
            case actionEnums.REMOVE:
                store.remove(props.id, props.parent)
                    .then(()=>{
                        props.onClose()
                    })
                return;
            default:
                return;

        }
    }


    return (
        <div className={clsx(styles.popUp_wrapper,{
            [styles.popUp_del_wrapper]: props.oneItem
        })} key={props.id}>
            {!props.oneItem &&
            <div onClick={(e) => handleAction(actionEnums.ADD, e, "child")}>
                <span>ADD ITEM</span>
            </div>
            }
            {!props.oneItem &&
            <div onClick={(e) => handleAction(actionEnums.ADD, e, "parent")}>
                <span>ADD LIST</span>
            </div>
            }

            <div onClick={(e) => handleAction(actionEnums.REMOVE, e)}>
                <span>DEL</span>
            </div>

        </div>
    )
}

export default ActionPopupMenu
