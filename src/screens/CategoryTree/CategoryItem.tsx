import React from 'react';
import styles from './CategoryTree.module.scss'
import CategoryList from './CategoryList'
import {INode} from '../../store/store'
import ActionPopupMenu from "./PopUpMenu/ActionPopupMenu";
import useCategoryLogic from './useCategoryLogic'



interface IProps {
    node: INode,
    isNodeMenuOpen: string,
    setIsNodeMenuOpen: any,
    parent: string,
    handleUpdateName: any
    handleRemove: any
}


function CategoryItem(props: IProps) {
    const {
        setIsOpen,
        canEdit,
        setCanEdit,
        name,
        setName,
        handleOpenMenu,
        updateName} = useCategoryLogic(props)


    // if the item has children or is a parent item - then call again the parent component
    if(props.node.type === "parent") {
        return <CategoryList
            node={props.node}
            isNodeMenuOpen={props.isNodeMenuOpen}
            setIsNodeMenuOpen={props.setIsNodeMenuOpen}
            handleUpdateName={props.handleUpdateName}
            handleRemove={props.handleRemove}

        />
    }

    const dynamicStyle = {width:`${name.length*10}px`, backgroundColor: canEdit? "#FFF": "#d9ebf7"}
    return (
        <li key={props.node.id}>

            <input
                onChange={(e)=>setName(e.target.value)}
                onKeyPress={(e)=> updateName(e.code)}
                onClick={(e)=> setCanEdit((s)=>!s)}
                onBlur={()=>updateName("Enter")}
                style={dynamicStyle}
                maxLength={20}
                readOnly={canEdit}
                className={styles.child}
                value={name}
                onContextMenu={(e)=>handleOpenMenu(e, props.node.id)}
            />
            <div className={styles.menu_wrapper}>
                {props.isNodeMenuOpen === props.node.id &&
                <ActionPopupMenu
                    parent={props.node.parentId}
                    id={props.node.id}
                    onClose={()=>props.setIsNodeMenuOpen("")}
                    setIsOpen={setIsOpen}
                    oneItem={true}
                /> }
            </div>

        </li>
    )


}
export default CategoryItem;
