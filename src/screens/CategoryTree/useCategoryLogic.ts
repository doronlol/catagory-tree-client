import React, {useEffect, useState, useRef} from 'react';
import styles from './CategoryTree.module.scss'
import SvgMinus from "../../assets/svgIcons/minus";
import SvgPlus from "../../assets/svgIcons/pluse";
import SvgEdit from "../../assets/svgIcons/edit";
import ActionPopupMenu from './PopUpMenu/ActionPopupMenu'
import CategoryItem from './CategoryItem'
import {INode} from '../../store/store'
import {useObserver} from 'mobx-react-lite'


interface IProps {
    node: INode,
    isNodeMenuOpen: string,
    setIsNodeMenuOpen: any,
    handleUpdateName: any
    handleRemove: any
}


function useCategoryLogic(props: IProps) {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [canEdit, setCanEdit] = useState<boolean>(true);
    const [name, setName] = useState<string>("");


    useEffect(()=>{
        if(props.node.name.length){
            setName(props.node.name)
        }

        document.addEventListener("click", (event) => {handleCloseMenu(event)});

        return ()=>{
            document.removeEventListener("click",(event) => {handleCloseMenu(event)} );
        }
    },[])

    const handleCloseMenu = (e: any)=>{
        e.stopPropagation();
        if(props.isNodeMenuOpen) props.setIsNodeMenuOpen("");
    }
    const handleOpenMenu = (e: any, id: string) =>{
        e.stopPropagation();
        e.preventDefault()
        if(props.isNodeMenuOpen !== id) props.setIsNodeMenuOpen(id);
    }

    const updateName = (code: string)=>{
        if(code === "Enter"){
            props.handleUpdateName(props.node.id, name).then(()=>{
                setCanEdit(true)
            }).catch((e:any)=>{
                console.log("failed to update name", e)
            })
        }
    }


    return {isOpen, setIsOpen,canEdit, setCanEdit, name, setName, handleCloseMenu, handleOpenMenu, updateName}


}

export default useCategoryLogic;
