import React  from 'react';
import styles from './CategoryTree.module.scss'
import SvgMinus from "../../assets/svgIcons/minus";
import SvgPlus from "../../assets/svgIcons/pluse";
import ActionPopupMenu from './PopUpMenu/ActionPopupMenu'
import CategoryItem from './CategoryItem'
import {INode} from '../../store/store'
import {useObserver} from 'mobx-react-lite'
import useCategoryLogic from "./useCategoryLogic";


interface IProps {
    node: INode,
    isNodeMenuOpen: string,
    setIsNodeMenuOpen: any,
    handleUpdateName: any
    handleRemove: any
}


function CategoryList(props: IProps) {
    const {
        isOpen,
        setIsOpen,
        canEdit,
        setCanEdit,
        name,
        setName,
        handleOpenMenu,
        updateName} = useCategoryLogic(props)

    const handleToggle = (e: any)=>{
        e.stopPropagation()
        setIsOpen((s)=>!s)
    }

    const generateChildElement =()=>{
        if(props.node.children && props.node.children.length){
            return props.node.children.map(child=>{
                return <CategoryItem
                    key={child.id}
                    node={child}
                    isNodeMenuOpen={props.isNodeMenuOpen}
                    setIsNodeMenuOpen={props.setIsNodeMenuOpen}
                    parent={props.node.id}
                    handleRemove={props.handleRemove}
                    handleUpdateName={props.handleUpdateName}
                />
            })
        }

        return <></>

    }

    let icon = !isOpen ? <SvgPlus/> : <SvgMinus/>

    const dynamicStyle = {width:`${name.length*10}px`, backgroundColor: canEdit? "#FFF": "#d9ebf7"}

    return useObserver(()=>
        <li className={styles.parent}
            key={props.node.id}

        >
            <div className={styles.icon_wrapper} onClick={handleToggle}>
                {icon}
            </div>

            <input
                onChange={(e)=>setName(e.target.value)}
                onKeyPress={(e)=> updateName(e.code)}
                onClick={(e)=> setCanEdit((s)=>!s)}
                onBlur={()=>updateName("Enter")}
                style={dynamicStyle}
                maxLength={20}
                readOnly={canEdit}
                className={styles.parent_text}
                value={name}
                onContextMenu={(e)=>handleOpenMenu(e, props.node.id)}
            />
            <div className={styles.menu_wrapper}>
                {props.isNodeMenuOpen === props.node.id &&
                <ActionPopupMenu
                    parent={props.node.parentId}
                    id={props.node.id}
                    onClose={()=>props.setIsNodeMenuOpen("")}
                    setIsOpen={setIsOpen}
                /> }
            </div>

            <ul className={styles.sub_parent}>
                {isOpen && generateChildElement()}
            </ul>

        </li>
    )
}

export default CategoryList;
