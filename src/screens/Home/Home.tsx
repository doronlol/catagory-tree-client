import React, {useEffect, useState} from 'react';
import CategoryTree from '../CategoryTree/CategoryTree'
import {useObserver} from 'mobx-react-lite'

function Home(){

    return useObserver(()=>
        <div className="App">
            <CategoryTree/>
        </div>
    )

}

export default Home;

